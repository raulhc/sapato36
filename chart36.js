/* jshint undef: true, unused: true */
/* global  $, Chart, myNewChart */

//<![CDATA[ 
$(window).load(function () {
    /*************************************************************************/
    /* boxes */
    var boxConf = function (incremento, necessario, obtido, legenda, percentagemSegura, pre, pos, tooltipRealizada, tooltipProjetada) {
		$(function() {
			$( document ).tooltip();
		});
        var data = [{
            label: "Obtido"
        }, {
            color: "#E2E2E2",
            highlight: "#E2E2E2",
            label: "Pendente"
        }];
        var options = {
            animation: false,
            percentageInnerCutout: 80
        };

        data[0].value = Math.floor(obtido);
        var necessarioRound = Math.ceil(necessario);
        data[1].value = Math.ceil(necessarioRound - data[0].value);
        if (data[1].value < 0) {
            data[1].value = 0;
        }

        if (data[0].value >= necessarioRound) {
            data[0].color = "#46BFBD";
            data[0].highlight = "#5AD3D1";
        } else if (data[0].value >= necessarioRound * percentagemSegura) {
            data[0].color = "#FDB45C";
            data[0].highlight = "#FFC870";
        } else {
            data[0].color = "#F7464A";
            data[0].highlight = "#FF5A5E";
        }

        $("section").append('<article id="box-' + incremento + '"><h3>' + legenda + '</h3><div id="js-mouse-bubbles" class="canvas-holder canvas-node-demo box-info"><canvas  height="250" width="400"></canvas><input style="color:#505050;" type="text" class="valor" readonly/><input type="text" class="descricao" readonly/></div><p title="'+tooltipRealizada+'"><b>Obtido: </b> '+ pre  + data[0].value + pos +'</p><p title="'+tooltipProjetada+'"> <b>Esperado: </b>' + pre + necessarioRound + pos +'</p>');
        $('#box-' + incremento + ' .valor').val(Math.floor(100 * data[0].value / necessarioRound) + '%');
        /*$('#box-' + incremento + ' .descricao').val('Realizado');*/
        if (data[0].value > necessarioRound) {
            $('#box-' + incremento + ' .valor').addClass('overgreen');
            options.percentageInnerCutout = 70;
        }

        //Get the context of the canvas element we want to select
        var c = $('#box-' + incremento + ' canvas');
        var ct = c.get(0).getContext('2d');

        myNewChart = new Chart(ct).Doughnut(data, options);
    };

    $("#button").click(function () {
        $('.container').empty();
        /*** Configuração ***/
        var isInRange = function (n, min, max) {
            if (n <= min) {
                return min;

            }
            if (max !== undefined) {
                if (n >= max) {
                    return max;
                }
            }
            return n;

        };
        $.getJSON($('#meta').val(), function (config) {

            $.getJSON('json/movimentacoes/rualhcmain.json', function (unlock) {
                $.getJSON('json/movimentacoes/raulhccontributions.json', function (unlockApoios) {
                    config.local.arrecadado.mensalmente = unlock.total_value;
                    config.local.pagantes.numeroTotal = unlock.total_contributions;
                    config.local.arrecadado.emCaixa = 1171.03;


                    var taxaCaptacaoFundos = (config.local.arrecadado.mensalmente * config.padrao.taxaPorArrecadacao) + (config.local.pagantes.numeroTotal * config.padrao.taxaPorTransferencia);
                    var arrecadadoMensalmenteUtil = config.local.arrecadado.mensalmente - taxaCaptacaoFundos;

                    //1-Básico mensal - tem limite

                    var custosBasicos = config.local.gasto.aluguel + config.local.gasto.saneamento + config.local.gasto.impostos + config.local.gasto.eletricidade + config.local.gasto.internet + config.local.gasto.telefone + config.local.gasto.outros + config.local.gasto.contador;
                    var custosBasicosArrecadados = isInRange(arrecadadoMensalmenteUtil, 0, custosBasicos);

                    //Variaveis auxiliares das proximas 2 saidas
                    var acessibilidade = config.local.pagantes.valorSalarioMinimo * config.padrao.percentagemAcessibilidade;
                    var descentralizacao = custosBasicos * config.padrao.percentagemDescentralizacao;
                    var mensalidadeRecomendada = Math.min(acessibilidade, descentralizacao);

                    //3-Descentralização - não tem limite
                    var numeroDePagantes = config.local.pagantes.numeroTotal;
                    var numeroDePagantesAcessibilidade = 0; //ainda será processada

                    //4-Acessibilidade - não tem limite	
                    var arrecadadoMensalmenteDescentralizado = 0;

                    // Processamento das mariáveis de descentralização e acessibilidade 
                    for (var i = 0; i < numeroDePagantes; i++) {
                        if (unlockApoios[i].value <= mensalidadeRecomendada) {
                            arrecadadoMensalmenteDescentralizado += unlockApoios[i].value;
                            numeroDePagantesAcessibilidade += 1;
                        }

                    }
                    var arrecadadoMensalmenteUtilDescentralizado = arrecadadoMensalmenteDescentralizado - taxaCaptacaoFundos; 
                    var arrecadadoMensalmenteUtilCentralizado = arrecadadoMensalmenteUtil - arrecadadoMensalmenteUtilDescentralizado;
                    //fim do processamento

                    //2-Reserva mensal - não tem limite
                    var reservaMensal = (custosBasicos * config.padrao.percentagemBasicoMensal) + (arrecadadoMensalmenteUtilCentralizado * config.padrao.percentagemCentralizadoMensal);
                    var reservaMensalArrecadada = isInRange((arrecadadoMensalmenteUtil - custosBasicos), 0);

                    var custoAnual = custosBasicos * 12;
                    var arrecadadoAnualCentralizado = arrecadadoMensalmenteUtilCentralizado * 12;

                    //6-Caixa de emergência - tem limite

                    var reservaBasica = (custoAnual * config.padrao.percentagemBasicoAnual) + (arrecadadoAnualCentralizado * config.padrao.percentagemCentralizadoAnual);
                    var reservaBasicaArrecadada = isInRange((config.local.arrecadado.emCaixa), 0, reservaBasica); 
                    //6-Caixa criativo - não tem limite
                    var reservaCriativa = custoAnual * config.padrao.percentagemReservaCriativa;
                    var reservaCriativaArrecadada = isInRange((config.local.arrecadado.emCaixa - reservaBasica), 0);


                    /***Saída****/
                    boxConf(0, custosBasicos, custosBasicosArrecadados, 'Básico mensal', 0.9, 'R$ ',',00','Valor obtido a partir do unlock. Subtraindo-se as taxas do moip (pagamento online). Quando o valor ultrapassa a \'meta projetada\' o que sobra vai para reserva mensal.','Custos fixos mensais (cada meta tem um custo fixo específico).');

                    boxConf(1, reservaMensal, reservaMensalArrecadada, 'Reserva mensal', 0.9, 'R$ ',',00','Quando o obtido do \'básico mensal\' maior que o esperado o sobressalente vem para esta reserva para imprevistos.','Este valor diminue conforme a descentralização da receita aumenta.');
                    boxConf(2, Math.floor(arrecadadoMensalmenteUtil), arrecadadoMensalmenteUtilDescentralizado, 'Descentralização', 0.9,'R$ ',',00','É um sub-conjunto da doação mensal. Aqui constará todo valor obtido por micro-apoios (apoios abaixo de R$ 39).','Todos os valores obtidas.'); 

                    boxConf(3, numeroDePagantes, numeroDePagantesAcessibilidade, 'Acessibilidade', 0.9, '',' micro-apoiadores','Micro-apoiadores (pessoas que fazem apoios abaixo de um valor específico).','Todos os apoiadores.'); 

                    boxConf(4, reservaBasica, reservaBasicaArrecadada, 'Caixa de emergência', 0.9, 'R$ ',',00','Valor que está em caixa no momento (conta no banco)','Este valor diminue sensívelmente conforme a descentralização da receita aumenta.');
                    boxConf(5, reservaCriativa, reservaCriativaArrecadada, 'Caixa criativo', 0.9, 'R$ ',',00','','');
                });
            });
        });
        return false;

    });

}); //]]>
