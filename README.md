# Sapato36

É o sistema de controle e transparência financeira do Raul Hacker Club

## Instruções

Basta clonar o repositório para /var/www/sapato36

Configurar domínio virtual para esse endereço

Colocar as instruções abaixo no cron:
```
0 5 * * * mv /var/www/sapato36/unlock.json /var/www/sapato36/unlock.json.old ; wget https://unlock.fund/pt-BR/raulhc.json -O /var/www/sapato36/unlock.json > /dev/null
0 5 * * * mv /var/www/sapato36/apoio.unlock.json /var/www/sapato36/apoio.unlock.json.old ; wget https://unlock.fund/pt-BR/initiatives/138/contributions.json -O apoio.unlock.json > /dev/null
```
